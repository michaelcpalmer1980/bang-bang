#include <vector>

class Server {
	private:
		// variables for socket interface
		int server_fd, new_socket, valread, int_port;
		struct sockaddr_in address;
		int opt = 1;
		int addrlen = sizeof(address);
		char buffer[1024] = { 0 };
		int locked = 1; // commands locked until security passed
		std::string secret;

		// setup server (called by contructor)
		void init(void);
		void load_secret(void);
		void get_net_options(void);

	public:
		Server();
		~Server();
		void kill(void);
		char *get_message(void); // await and return a message
		std::vector<std::string> poll_messages(void);
		int looping = 1;
};

// must only declare the functions once
#ifndef SERVER_HEADER
#define SERVER_HEADER

void event_handler(int s);
void set_handler(void);

#endif // MY_HEADER_H_INC
