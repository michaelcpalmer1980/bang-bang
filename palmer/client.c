// Client side C/C++ program to demonstrate Socket programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#define PORT 8080

int main(int argc, char const* argv[])
{
	int sock = 0, valread, client_fd;
	struct sockaddr_in serv_addr;
	char buffer[1024] = { 0 };

	// Read network enviroment variables
	char *ip_addr = getenv("LAZ_IP_ADDR");
	char *ip_port = getenv("LAZ_IP_PORT");
	// Set network options to user or default options
	ip_addr = (ip_addr) ? ip_addr : "127.0.0.1";
	int int_port = PORT;
	if (ip_port) {
		int_port = atoi(ip_port);
	}
	printf("ADDR: %s, PORT: %d\n", ip_addr, int_port);

	// Create network socket and set port
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(int_port);

	// Convert IPv4 and IPv6 addresses from text to binary form
	if (inet_pton(AF_INET, ip_addr, &serv_addr.sin_addr)
		<= 0) {
		printf(
			"\nInvalid address/ Address not supported \n");
		return -1;
	}

	// Connect to socket or exit on fail
	if ((client_fd
		= connect(sock, (struct sockaddr*)&serv_addr,
				sizeof(serv_addr)))
		< 0) {
		printf("\nConnection Failed \n");
		return -1;
	}

	// Load message from command line or set default
	char *message;
	if (argc <= 1) {
		// display help message if now parameters given
		printf("Use LAZ_IP_ADDR and LAZ_IP_PORT env variables to change options.\n");
		message = "Default Message";
	}
	else {
		message = (char *)argv[1];
	}
	
	// send message and display in console
	send(sock, message, strlen(message), 0);
	printf("Message sent: %s\n", message);
	close(client_fd);
	return 0;
}
